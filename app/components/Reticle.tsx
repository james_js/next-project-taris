export default function Reticle(props: { mesh: any }) {
	return (
			<mesh ref={props.mesh} rotation-x={-Math.PI / 2}>
				<torusGeometry args={[0.1, 0.01, 3, 25]} />
				<meshBasicMaterial color={"black"} />
			</mesh>
	);
}
