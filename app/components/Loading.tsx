import { Html } from "@react-three/drei";

export default function Loading() {
	return (
		<Html>
			<h2>Loading...</h2>
		</Html>
	);
}
