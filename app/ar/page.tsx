import dynamic from "next/dynamic";

const AR = dynamic(() => import("./AR"), { ssr: false });

export default function App() {
	return (
			<AR />
	);
}
