"use client";

import { Suspense, useRef, useState } from "react";
import {
	Interactive,
	XR,
	ARButton,
	Controllers,
	useXR,
	useHitTest,
	RayGrab,
} from "@react-three/xr";
import { Text, Html, OrbitControls } from "@react-three/drei";
// import "./styles.css";
import { Canvas, useFrame, useThree } from "@react-three/fiber";

import Model from "../components/car-parts/Model";

import Loading from "../components/Loading";
// @ts-ignore
import WebXRPolyfill from "webxr-polyfill";
import Reticle from "../components/Reticle";
import * as THREE from "three";
import { useModelActions, useNumClicks, useProperties } from "../store/arStore";

function Object() {
	const { initialized, position } = useProperties();

	const { initialize, reposition, modelClicked } = useModelActions();

	// non-null assertion so we don't null-check in later effects
	const mesh = useRef<THREE.Mesh>(null!);

	//empty Qat as we don't need the hit matrix angle applied to our reticle
	let hitMatrixQat = new THREE.Quaternion();
	useHitTest((hitMatrix: THREE.Matrix4, hit: XRHitTestResult) => {
		// use hitMatrix to position any object on the real life surface
		hitMatrix.decompose(
			mesh.current.position,
			hitMatrixQat,
			mesh.current.scale
		);
	});

	const onSelect = () => {
		// @ts-ignore
		console.log("Model loaded!");
		initialize();
		reposition(mesh.current.position);
	};

	//   const { modelClicked } = useModelActions();

	const modelSelected = () => {
		modelClicked();
	};

	return (
		<>
			<Interactive onSelect={onSelect}>
				<Reticle mesh={mesh} />
			</Interactive>
			<Interactive onSelect={modelSelected}>
				{initialized && <Model position={position} />}
			</Interactive>
		</>
	);
}

function ARScene() {
	return (
		<>
			<ambientLight />
			<pointLight position={[10, 10, 10]} />

			<Object />

			{/* <OrbitControls /> */}

			<Controllers />
		</>
	);
}

export default function AR() {
	// @ts-ignore
	const polyfill = new WebXRPolyfill();

	const [color, setColor] = useState("blue");

	const handleColorClick = () => {
		// @ts-ignore
		setColor(Math.random() * 0xffffff);
	};

	return (
		<>
			<ARButton
			// sessionInit={{
			// 	requiredFeatures: ["hit-test"],
			// }}
			// enterOnly
			/>
			{/* {(status) => `WebXR ${status}`} */}
			{/* </ARButton> */}
			<Canvas>
				<Suspense fallback={<Loading />}>
					<XR referenceSpace="local">
						<ARScene />
					</XR>
				</Suspense>
			</Canvas>
		</>
	);
}
