import * as THREE from "three";
import { create } from "zustand";

type ARState = {
  numClicks: number;
  wheels: {
    frontLeftPopped: boolean;
    frontRightPopped: boolean;
    backLeftPopped: boolean;
    backRightPopped: boolean;
    backLeftOrigin: THREE.Vector3 | undefined;
    frontRightOrigin: THREE.Vector3 | undefined;
    frontLeftOrigin: THREE.Vector3 | undefined;
    backRightOrigin: THREE.Vector3 | undefined;
  };
  properties: {
    // scale: number;
    initialized: boolean;
    position: THREE.Vector3;
  };
  highlights: {
    bonnet: boolean;
    roof: boolean;
    windscreen: boolean;
    doorLeft: boolean;
    doorRight: boolean;
    middleClip: boolean;
  };
  log: string[];

  actions: {
    modelClicked: () => void;
    initialize: () => void;
    // scaleModel: (value: number) => void;
    reposition: (position: THREE.Vector3) => void;
    popFrontLeft: () => void;
    popFrontRight: () => void;
    popBackLeft: () => void;
    popBackRight: () => void;
    setWheelOriginBL: (position: THREE.Vector3) => void;
    // setWheelOriginFL: (position:THREE.Vector3)=> void;
    // setWheelOriginBR: (position:THREE.Vector3)=> void;
    setWheelOriginFR: (position: THREE.Vector3) => void;
    setWheelOriginBR: (position: THREE.Vector3) => void;
    setWheelOriginFL: (position: THREE.Vector3) => void;

    toggleHighlight: {
      bonnet: () => void;
      roof: () => void;
      windscreen: () => void;
      doorLeft: () => void;
      doorRight: () => void;
      middleClip: () => void;
    };
  };
};

export const useARStore = create<ARState>()((set, get) => ({
  numClicks: 0,
  wheels: {
    frontLeftPopped: false,
    frontRightPopped: false,
    backLeftPopped: false,
    backRightPopped: false,
    backLeftOrigin: undefined,
    frontRightOrigin: undefined,
    frontLeftOrigin: undefined,
    backRightOrigin: undefined,
  },
  properties: {
    // scale: 0.02,
    position: new THREE.Vector3(0, 0, 0),
    initialized: false,
  },
  highlights: {
    bonnet: false,
    roof: false,
    windscreen: false,
    doorLeft: false,
    doorRight: false,
    middleClip: false,
  },
  log: [],

  actions: {
    modelClicked: () => set((state) => ({ numClicks: state.numClicks + 1 })),
    // decreaseCounterNumber: () => set((state) => ({ number: state.number - 1 })),
    initialize: () => {
      if (!get().properties.initialized)
        set((state) => ({
          properties: {
            ...state.properties,
            initialized: !state.properties.initialized,
          },
        }));
    },
    // scaleModel: (value) =>
    //   set((state) => ({ properties: { ...state.properties, scale: value } })),
    reposition: (position) =>
      set((state) => ({
        properties: { ...state.properties, position: position },
      })),
    popFrontLeft: () =>
      set((state) => ({
        wheels: {
          ...state.wheels,
          frontLeftPopped: !state.wheels.frontLeftPopped,
        },
      })),
    popFrontRight: () =>
      set((state) => ({
        wheels: {
          ...state.wheels,
          frontRightPopped: !state.wheels.frontRightPopped,
        },
      })),
    popBackLeft: () =>
      set((state) => ({
        wheels: {
          ...state.wheels,
          backLeftPopped: !state.wheels.backLeftPopped,
        },
      })),
    popBackRight: () =>
      set((state) => ({
        wheels: {
          ...state.wheels,
          backRightPopped: !state.wheels.backRightPopped,
        },
      })),
    setWheelOriginBL: (position) =>
      set((state) => ({
        wheels: { ...state.wheels, backLeftOrigin: position },
      })),
    setWheelOriginFR: (position) =>
      set((state) => ({
        wheels: { ...state.wheels, frontRightOrigin: position },
      })),
    setWheelOriginBR: (position) =>
      set((state) => ({
        wheels: { ...state.wheels, frontRightOrigin: position },
      })),
    setWheelOriginFL: (position) =>
      set((state) => ({
        wheels: { ...state.wheels, frontRightOrigin: position },
      })),
    toggleHighlight: {
      bonnet: () =>
        set((state) => ({
          highlights: { ...state.highlights, bonnet: !state.highlights.bonnet },
        })),
      roof: () =>
        set((state) => ({
          highlights: { ...state.highlights, roof: !state.highlights.roof },
        })),
      windscreen: () =>
        set((state) => ({
          highlights: {
            ...state.highlights,
            windscreen: !state.highlights.windscreen,
          },
        })),
      doorLeft: () =>
        set((state) => ({
          highlights: {
            ...state.highlights,
            doorLeft: !state.highlights.doorLeft,
          },
        })),
      doorRight: () =>
        set((state) => ({
          highlights: {
            ...state.highlights,
            doorRight: !state.highlights.doorRight,
          },
        })),
      middleClip: () =>
        set((state) => ({
          highlights: {
            ...state.highlights,
            middleClip: !state.highlights.middleClip,
          },
        })),
    },
  },
}));

export const useModelActions = () => useARStore((state) => state.actions);

export const useNumClicks = () => useARStore((state) => state.numClicks);

export const useWheels = () => useARStore((state) => state.wheels);

export const useProperties = () => useARStore((state) => state.properties);

export const usePartVisibility = () => useARStore((state) => state.highlights);
