"use client";

import "./globals.css";
import type { Metadata } from "next";
import { IBM_Plex_Mono } from "next/font/google";
import Script from "next/script";
import { useEffect, useState } from "react";

const ibm = IBM_Plex_Mono({
	weight: "400",
	subsets: ["latin"],
	display: "swap",
});

// export const metadata: Metadata = {
// 	title: "Project Taris",
// 	description: "Introducing WebXR capabilities",
// };

const Dynamic = ({ children }: { children: React.ReactNode }) => {
	const [hasMounted, setHasMounted] = useState(false);

	useEffect(() => {
		setHasMounted(true);
	}, []);

	if (!hasMounted) {
		return null;
	}

	return <>{children}</>;
};

export default function RootLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<html lang="en">
			{/* <Script src="https://launchar.app/sdk/v1?key=UpLyBDWhCA5ZwWlGMtXpwveO0F8JUuMd&redirect=true" /> */}
			<body className={ibm.className}>
				<Dynamic>{children}</Dynamic>
			</body>
		</html>
	);
}
