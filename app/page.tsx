import "./home.css";
import Navbar from "./components/Navbar";
import Link from "next/link";

export default function App() {
	return (
		<>
			<Navbar />
			<div className="brief">
				Project Taris, a series of demos to explore using web APIs for
				geospatial data visualisation.
			</div>
			<div>
				<h1>Demos</h1>
				<div className="demo-box">
					<h2>Car Customizer</h2>
					<p>
						Think of placing IKEA furniture through the camera and seeing on
						your phone where it best fits in your home, now imagine that with a
						car! Simply place the car then you can pick off all the parts and
						customize them!
					</p>
					<Link href="/ar" prefetch={false}>
						Demo Link
					</Link>
				</div>
				<div className="demo-box">
					<h2>Simple Map</h2>
					<p>
						A test page to demonstrate how to add a map to a website using the
						Google Maps API.
					</p>
				</div>
				<div className="demo-box">
					<h2>WebGL Map</h2>
					<p>
						A more advanced mapping project that renders a contextual map in the
						browser. This demo makes use of WebGl through the Three.js library
						to add a third dimension to Google maps.
					</p>
				</div>
				<div className="demo-box">
					<h2>WebXR Augmented Reality</h2>
					<p>
						This demo shows a simple Augmented Reality web app. It uses
						JavaScript to render 3D models that appear as if they exist in the
						real world, using the WebXR Device API that combines AR and
						virtual-reality (VR) functionality.
					</p>
				</div>
			</div>

			<div>
				<h1>About</h1>
				<div className="about">
					This page is a live repository for a variety of demos, examining the
					current capabilities of map APIs and WebXR. The aim is to expand the
					learnings from Project Coruscant, building on the proof of concept to
					create a scalable interactive visualiser.
					<div>https://github.com/pwc-uk-metaverse/project-taris</div>
				</div>
			</div>
		</>
	);
}
