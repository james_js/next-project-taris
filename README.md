## Local Dev Environment

In order to develop and test you will need to install ngrok in order to run a https tunnel giving you a way to access the dev server (which serves a http) from ios without SSL complaints.

https://ngrok.com/download
install using brew or macports

sign up and create an authtoken

Run npm run dev and then in a new terminal run ngrok http 3000 which will give you a url (new every time but similar to https://84d6-82-2-89-8.ngrok-free.app) in which you can access your dev runtime from ios safari

## Live deployment

The latest push to main can be viewed on your mobile device here: https://next-project-taris.vercel.app/

- (on ios) Variant Launch should pop up asking if you want to open the app in App Clip
- To make changes and view them live on your phone simply save the edited code and refresh the app launch broswer tab and relaunch the ar app.

## Intro

This is a MVP for getting WebXR applications (built using the modern react-three-fiber) running on iOS devices.
Here is a working example of what we want to achieve: https://2mcjdq.csb.app/ source: https://codesandbox.io/s/2mcjdq
The techstack is:

- Next.js react framework
- React-three-fiber (react wrapper for three.js 3D renderer)
- react-xr (https://github.com/pmndrs/react-xr) XR with react-three-fiber
- Variant Launch, utilises App Clips to launch an container environment containing your XR application from Safari on iOS

## Documentation

- R3F XR (https://github.com/pmndrs/react-xr)
- WebXR Device API (https://www.w3.org/TR/webxr/)
- Variant Launch (https://launch.variant3d.com/docs/)

## Resources

- Variant Launch three.js demo (https://demos.launchar.app/threejs) and source (https://github.com/Variant3d/launch-examples/tree/main/threejs)
